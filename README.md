# Pentothal.Tv


> Il tiopental sodico (nome commerciale Pentothal Sodium) è barbiturico ad azione ipnotica utilizzato nell'induzione dell'anestesia generale. 
> Appartiene alla sottoclasse dei tiobarbiturici, dove l'atomo di ossigeno viene sostituito con un atomo di zolfo.
> Il tiopental sodico è noto al grande pubblico con il nome di Pentothal, soprattutto grazie al cinema e alla televisione, per essere stato usato, 
> a dosaggi subanestetici, come siero della verità: ad esempio, ne fanno grande uso nelle loro avventure immaginarie Diabolik e Agente 47 e viene 
> adoperato anche da Hannibal Lecter nel film Hannibal Lecter: le origini del male.
> 
> da https://it.wikipedia.org/wiki/Tiopental_sodico

## init freedom

Pentothal.Tv aderisce e sostiene la campagna [init-freedom](https://devuan.org/os/init-freedom).

## servizi disponibili

- blog (wordpress)
- streaming audio/vide (icecast)

## servizi programmati

- email
- webmail (possibilità di scaricare e leggere la posta da altri provider)
- mailing list
- sincronizzazione file in cloud (stile dropbox, google drive etc.) 
- scrittura collaborativa